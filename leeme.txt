 Lama-2011
---------
0- Verificar si python2 est instalado en el sistema. 
Para ello escribir en l terminal:
python --version

>> Python 2.7.6
En caso de que no este instalado (raro), pero escribir:
sudo apt-get install python2 

--------------------------------------------------------------------------------------------
En caso de necesitar cambiar de python3 a python2 en el sistema, cuando tenemos instalado ambos:
cd /usr/bin
ls -l python
    lrwxrwxrwx 1 root root 7  17 Dec. 12:04 python -> python3
ln -sf python2 python
ls -l python
    lrwxrwxrwx 1 root root 10 Apr 11 14:28 python -> python2
---------------------------------------------------------------------------------------------


1- Clonar el repositorio git en algun directorio de su computadora:
git clone https://facundobustos@bitbucket.org/facundobustos/planning-conae-2020-lab.git

2- Luego en la terminal posicionarse en el directorio "gawk" del repositorio
y correr el script para instalar gawk:
cd gawk
./instalar_gawk.sh
cd .. 

3- Para ejecutar el planificador para un dominio y una tarea, tipear en la terminal:  
./execute_lama.sh <dominio> <nn>

<dominio> es el nombre de la carpeta donde se encuentran el "domain.pddl" que se quiere ejecutar 
<nn> la tarea que desea ejecutar para ese domino 

Aparecerán varias soluciones (planes) numerados, y el output de la resolución 
(con los datos para las estadísticas) en el archivo lama_output_$dominio$_$nn$
en la carpeta solutions/<dominio>/<nn>.

Ejemplo, si deseamos ejecutar el planificador para el dominio "elevators" en su tarea "01.pddl",
entonces en la terminal ejecutamos:
./execute_lama.sh elevators 01

Y el comando genera el archivo "lama_output_elevators_01" en el directorio "solutions/elevators/01/"

Nota: en el archivo script "execute_lama.sh" se configura con un timoeut que actualemente está configurado en 10 segundos.
 

