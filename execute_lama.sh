echo "Searching a plan ..."

mkdir -p ./solutions/$1/$2
cd ./solutions/$1/$2

timeout 10s ../../../planners/lama-2011/plan ../../../domains/$1/domain.pddl ../../../domains/$1/$2.pddl ./lama_$1_$2 > ./lama_output_$1_$2


echo "Done."
